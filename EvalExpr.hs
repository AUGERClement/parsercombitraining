import Text.Printf
import System.Exit

data Op = Add
        | Sub
        | Mult
        | Divi 
        | Power
        | Undefined deriving (Show)

type Value = Int
type Operator = Char

data Expr = Leaf Value
        | Node Op Expr Expr deriving (Show)


type Parser a = String -> Maybe (String, a)


start :: String -> IO ()
start xs = do
        case evalExpr xs of
                Just x -> printf "%.2i\n" x
                Nothing -> exitWith (ExitFailure 84)

evalExpr :: String -> Maybe Int
evalExpr [] = Nothing
evalExpr str = do
        val <- computeExpr <$> fillExpr str -- <$> is a fmap shortcut 
        return (val)


fillAST :: String -> Maybe Expr
fillAST [] = Nothing
fillAST str = case parseInt str of
        Nothing -> Nothing
        Just (rest, val) -> case parseOperator rest of
                Nothing -> Nothing
                Just (rest', op) -> case parseInt rest' of
                        Nothing -> Nothing
                        Just (rest'', val') -> Just $ Node (getOp op) (Leaf val) (Leaf val')

{--Fill an expression of type a op b (Use the monadic status of Maybe.)
fillRec :: String -> Maybe Expr
fillRec [] = Nothing
fillRec str = do
        (rest, val) <- parseInt str
        case parseOperator rest of
                Nothing -> return (Leaf val)
                Just (rest', op) ->  --If I find how it work, I may use <|>. Edit : Nope, not in the prelude...
        (rest'', val') <- parseInt rest'
        return (Node (getOp op) (Leaf val) (Leaf val'))
        --}

--Fill an expression of type a op b (Use the monadic status of Maybe.)
fillExpr :: String -> Maybe Expr
fillExpr [] = Nothing
fillExpr str = do
        (rest, val) <- parseInt str
        (rest', op) <- parseOperator rest --If I find how it work, I may use <|>. Edit : Nope, not in the prelude...
        (rest'', val') <- parseInt rest'
        return (Node (getOp op) (Leaf val) (Leaf val'))


computeExpr :: Expr -> Int
computeExpr (Leaf val) = val
computeExpr (Node op expr expr') = computeCalc op (computeExpr expr) (computeExpr expr')

computeCalc :: Op -> Int -> Int -> Int
computeCalc Add a b = a + b
computeCalc Sub a b = a - b
computeCalc Mult a b = a * b
computeCalc Divi a b = div a b
computeCalc Power a b = a ^ b
computeCalc Undefined a b = undefined

{--Old version can fit a Integer into a Expr.
fillAST :: String -> Maybe Expr
fillAST [] = Nothing
fillAST str = case parseInt str of
        Nothing -> Nothing
        Just (rest, val) -> Just (Leaf val)
--}

many :: Parser a -> Parser [a]
many parser = \s -> go [] s
        --Auxiliary function collecting parsed values.
        where go list s =
                case parser s of
                        Nothing -> Just (s, reverse list)
                        Just (rest, a) -> go (a:list) rest

--Try to find and parse one of the char.
oneOf :: [Char] -> Parser Char
oneOf [] parser = Nothing
oneOf validChars (x:xs)
        | x `elem` validChars = Just (xs, x)
        | otherwise = Nothing

parseDigit :: Parser Char
parseDigit [] = Nothing
parseDigit (x:xs)
        | x `elem` "0123456789" = Just (xs, x)
        | otherwise = Nothing

parseInt :: Parser Value
parseInt xs = case many parseDigit xs of
        Just (_, []) -> Nothing
        Just (rest, xs) -> Just (rest, read xs)
        Nothing -> Nothing

parseOperator :: Parser Operator
parseOperator [] = Nothing
parseOperator xs = oneOf "+-*/^" xs

getOp :: Operator -> Op
getOp '+' = Add
getOp '-' = Sub
getOp '*' = Mult
getOp '/' = Divi
getOp '^' = Power
getOp _ = Undefined