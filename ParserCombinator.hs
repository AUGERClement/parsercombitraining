type Parser a = String -> Maybe (String, a)

--Conseil de Caro
type Operator = Char
type Digit = Char
type Dot = Char

--Combinators

{--Try each parser of the list until one work
choice :: [Parser a] -> Parser a
choice (x:xs) = case x of
    Nothing -> choice xs
    Just _ -> x--}

--One or none
--Use the monadic Power of Maybe.
optionnal :: Parser a -> Parser (Maybe a)
optionnal parser = \s -> apply s
        where apply s =
                case parser s of
                        Nothing -> Just (s, Nothing)
                        Just (rest, a) -> Just (rest, Just (a))


--Try
try :: Parser a -> Parser a
try parser = parser

--Zero or more
many :: Parser a -> Parser [a]
many parser = \s -> go [] s
        --Auxiliary function collecting parsed values.
        where go acc s =
                case parser s of
                        Nothing -> Just (s, reverse acc)
                        Just (rest, a) -> go (a:acc) rest

--Try to find and parse one of the char.
oneOf :: [Char] -> Parser Char
oneOf [] parser = Nothing
oneOf validChars (x:xs)
        | x `elem` validChars = Just (xs, x)
        | otherwise = Nothing

--New some implementation : Use the monadic status of Maybe to chain it.
some :: Parser a -> Parser [a]
some parser = \s -> do
        (rest, first) <- parser s
        (rest', others) <- many parser rest
        return (rest', first:others)

--Ultimate some implementation : Use both the monadic status of Maybe, and the binding (>>=) operator.
bindSome :: Parser a -> Parser [a]
bindSome parser = undefined

{--Old imprementaton of some : One or more
some :: Parser a -> Parser [a]
some parser = \s -> apply [] s
        where apply list s =
                case parser s of
                    Just (rest, a) -> apply (a:list) rest
                    Nothing -> case list of --If list == [] then Nothing, else like many
                                [] -> Nothing
                                x -> Just (s, reverse x)
                                --}

--Use successively Parser a, Parser Sep, and once more Parser a
--Use the Monadic power of Maybe ?
twoWithSep :: Parser a -> Parser b -> Parser c
twoWithSep parserA parserB = undefined

--Parsers

parseDigit :: Parser Digit
parseDigit [] = Nothing
parseDigit xs = oneOf "012345789" xs

parseInt :: Parser Int
parseInt [] = Nothing
parseInt xs = case some parseDigit xs of
        Just (rest, xs) -> Just (rest, read xs)
        Nothing -> Nothing

parseDot :: Parser Dot
parseDot [] = Nothing
parseDot (x:xs)
    | x == '.' = Just (xs, '.')
    | otherwise = Nothing

parseFloat :: Parser Float
parseFloat [] = Nothing
parseFloat xs = do
        (rest, first) <- some parseDigit xs
        (rest', Just (dot)) <- optionnal parseDot rest
        (rest'', Just (second)) <- optionnal (some parseDigit rest')
        return (rest', read (first ++ (dot:second)))

parseOperator :: Parser Operator
parseOperator [] = Nothing
parseOperator xs = oneOf "+-*/^" xs